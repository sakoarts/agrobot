# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/mnt/catkin_ws/src/dynamixel-workbench/dynamixel_workbench_toolbox/include".split(';') if "/mnt/catkin_ws/src/dynamixel-workbench/dynamixel_workbench_toolbox/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;dynamixel_sdk".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ldynamixel_workbench_toolbox".split(';') if "-ldynamixel_workbench_toolbox" != "" else []
PROJECT_NAME = "dynamixel_workbench_toolbox"
PROJECT_SPACE_DIR = "/mnt/catkin_ws/devel"
PROJECT_VERSION = "2.0.0"
